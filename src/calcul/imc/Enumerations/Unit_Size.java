/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package calcul.imc.Enumerations;

/**
 *
 * @author wslowikowski
 */
public enum Unit_Size { 
    M("m",1), CM("cm",0.01), INCH("in",0.39370079);
    
    private final String unit;
    private final double coeffToM;
    
   Unit_Size(String _unit, double _coeffToM){
   this.unit = _unit;
   this.coeffToM = _coeffToM;
   }
   public String getUnit(){
       return unit;
   }
   public double getCoeffToM() {
       return coeffToM;
   } 
    public static Unit_Size getSizeFromUnit(String unit){
        for (Unit_Size c : values()){
            if (c.unit.toUpperCase().equals(unit.toUpperCase())){
                return c;
            }
        }
        return null;
    }
    public static String toStringUnits(){ 
        String res = "";
        Unit_Size[] tabUs = values();
        for (int i = 0; i<tabUs.length; i++){
            Unit_Size us = tabUs[i];
            if(i == tabUs.length -1){
                res += us.getUnit();
            }
            else{
            res += us.getUnit() + ",";
            }
        }
        
        return res;
    }
}
