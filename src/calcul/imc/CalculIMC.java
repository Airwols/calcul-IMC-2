/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package calcul.imc;

import calcul.imc.Enumerations.Unit_Size;
import calcul.imc.Enumerations.Unit_Weight;
import static java.time.Clock.system;
import java.util.Scanner;

/**
 *
 * @author William SLOWIKOWKSI
 */
/*Nom de la classe , ici CalculIMC*/
public class CalculIMC {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        //declration des variables
        double poids;
        double taille;
        double calculimc = 0.0;

        Scanner scanner = new Scanner(System.in);
        String result = "";

        System.out.println("Voulez vous calculer votre IMC ? O pour oui / N pour non");
        if (scanner.hasNextLine()) {
            result = scanner.nextLine();
            if ("O".equals(result.toUpperCase())) {

            } else if ("N".equals(result.toUpperCase())) {
                System.exit(0);
            }
            // Afficher le message"Entrez un poids"
            poids = askDouble(scanner, "poids","kg");
            taille = askDouble(scanner, "taille","m");
            
           /* String valeurpoids;
            System.out.println("Entrez votre poids en kg : ");
            valeurpoids = scanner.nextLine();
            valeurpoids = valeurpoids.replace(",", ".");
            poids = Double.parseDouble(valeurpoids);

            String valeurtaille;
            System.out.println("Entrez votre taille en cm : ");
            valeurtaille = scanner.nextLine();
            valeurtaille = valeurtaille.replace(",", ".");
            taille = Double.parseDouble(valeurtaille);*/

            calculimc = poids / (taille * taille);
            System.out.println("imc est de " + calculimc);
            if (calculimc < 16.5) {
                System.out.println("votre IMC est de : " + calculimc + ", vous êtes en dénutrition ou en anorexie");
            } else if (calculimc <= 16.5 && calculimc < 18.5) {
                System.out.println("votre IMC est de : " + calculimc + ", vous êtes maigre");
            } else if (calculimc <= 18.5 && calculimc < 25) {
                System.out.println("votre IMC est de : " + calculimc + ", vous êtes de corpulence normale");
            } else if (calculimc <= 25 && calculimc < 30) {
                System.out.println("votre IMC est de : " + calculimc + ", vous êtes en surpoids");
            } else if (calculimc <= 30 && calculimc < 35) {
                System.out.println("votre IMC est de : " + calculimc + ", vous êtes en obésité modérée");
            } else if (calculimc <= 35 && calculimc < 40) {
                System.out.println("votre IMC est de : " + calculimc + ", vous êtes en obésité sévère");
            } else if (calculimc > 40) {
                System.out.println("votre IMC est de : " + calculimc + ", vous êtes en obésité morbide");
            }

        }
    }
    
    public static double askDouble (Scanner sc, String type, String unit ){
        String valeur;
        System.out.println("Entrez votre " + type + " en " + unit);
            valeur = sc.nextLine();
            valeur = valeur.replace(",", ".");
        double value = Double.parseDouble(valeur);
        getUnit(sc, type);
        return value;
    }
    public static String getUnit(Scanner scv, String type){
        
        if ("POIDS".equals(type.toUpperCase())){
            String valeurpds;
            System.out.println("Veuillez choisir l'unité dans laquelle vous avez saisi votre "+ type + "vous avez le choix entre les valeurs suivantes");
            System.out.println(Unit_Weight.toStringUnits());
            valeurpds = scv.nextLine();
            
        }else if ("TAILLE".equals(type.toUpperCase())){
            String valeurhau;
            System.out.println("Veuillez choisir l'unité dans laquelle vous avez saisi votre "+ type + "vous avez le choix entre les valeurs suivantes");
            System.out.println(Unit_Size.toStringUnits());
            valeurhau = scv.nextLine();
        }
        return null;
    }
}
